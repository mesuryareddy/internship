from flask import Flask, render_template, request, redirect, url_for, flash, jsonify
from pymongo import MongoClient
from bson.objectid import ObjectId
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import os
from werkzeug.utils import secure_filename
import zlib

app = Flask(__name__)
app.secret_key = 'your_secret_key'

client = MongoClient("mongodb://localhost:27017/")
db = client["demographics"]
collection = db["citizens"]

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

app.config['UPLOAD_FOLDER'] = 'static/uploads/profile_pics'
os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)

def deflate(data):
    data_bytes = data.encode('utf-8')
    compressed_data = zlib.compress(data_bytes)
    return compressed_data

def inflate(compressed_data):
    data_bytes = zlib.decompress(compressed_data)
    data = data_bytes.decode('utf-8')
    return data

class User(UserMixin):
    def __init__(self, user_id, username, password, profile_pic_url=None):
        self.id = user_id
        self.username = username
        self.password = password
        self.profile_pic_url = profile_pic_url

@login_manager.user_loader
def load_user(user_id):
    user_data = collection.find_one({"_id": ObjectId(user_id)})
    if user_data:
        return User(
            user_id=str(user_data["_id"]),
            username=inflate(user_data["username"]),
            password=inflate(user_data["password"]),
            profile_pic_url=user_data.get("profile_pic_url")
        )
    return None

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        confirm_password = request.form['confirm_password']
        profile_pic = request.files['profile_pic']

        if password != confirm_password:
            flash('Passwords do not match')
            return redirect(url_for('register'))

        existing_user = collection.find_one({"username": deflate(username)})
        if existing_user:
            flash('Username already exists')
            return redirect(url_for('register'))

        if profile_pic:
            filename = secure_filename(profile_pic.filename)
            profile_pic_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            profile_pic.save(profile_pic_path)
            profile_pic_url = f'uploads/profile_pics/{filename}'
        else:
            profile_pic_url = None

        collection.insert_one({
            "username": deflate(username),
            "password": deflate(password),
            "profile_pic_url": profile_pic_url
        })

        flash('Registration successful, please login')
        return redirect(url_for('login'))

    return render_template('register.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user_data = collection.find_one({"username": deflate(username)})
        if user_data and inflate(user_data['password']) == password:
            user = User(
                user_id=str(user_data["_id"]),
                username=inflate(user_data["username"]),
                password=inflate(user_data["password"]),
                profile_pic_url=user_data.get("profile_pic_url")
            )
            login_user(user)
            return redirect(url_for('dashboard'))
        else:
            flash('Invalid username or password')
    return render_template('login.html')

# Predefined username and password
PREDEFINED_USERNAME = "admin"
PREDEFINED_PASSWORD = "password123"

def deflate(data):
    return zlib.compress(data.encode('utf-8'))

def inflate(data):
    return zlib.decompress(data).decode('utf-8')

@app.route('/clickhere', methods=['GET', 'POST'])
def clickhere():
    if request.method == 'POST':
        empusername = request.form['empusername']
        password = request.form['password']

        # Check against predefined credentials first
        if empusername == PREDEFINED_USERNAME and password == PREDEFINED_PASSWORD:
            flash('Login successful with predefined credentials')
            return redirect(url_for('rto_dashboard'))

        compressed_username = deflate(empusername)
        compressed_password = deflate(password)

        user_data = collection.find_one({"username": compressed_username})

        if user_data and inflate(user_data['password']) == password:
            user = User(
                user_id=str(user_data["_id"]),
                username=inflate(user_data["username"]),
                password=inflate(user_data["password"]),
                profile_pic_url=user_data.get("profile_pic_url")
            )
            clickhere(user)
            return redirect(url_for('rto_dashboard'))
        else:
            flash('Invalid username or password')

    return render_template('clickhere.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/')
@login_required
def index():
    return redirect(url_for('dashboard'))

@app.route('/dashboard')
@login_required
def dashboard():
    user_info = {
        "username": current_user.username,
        "profile_pic_url": current_user.profile_pic_url
    }
    return render_template('dashboard.html', **user_info)

@app.route('/status')
@login_required
def check_application_status():
    user = collection.find_one({"_id": ObjectId(current_user.id)})
    if user:
        user['_id'] = str(user['_id'])
    return render_template('status.html', user=user)

@app.route('/get_details', methods=['POST'])
@login_required
def get_details():
    aadhaar_number = request.form['aadhaarNumber']
    user = collection.find_one({"aadhaar_number": aadhaar_number})
    if user:
        user['_id'] = str(user['_id'])
        return jsonify(user)
    return jsonify({"error": "User not found"})

@app.route('/profile')
@login_required
def profile():
    return render_template('profile.html')

@app.route('/book_date_time/<user_id>', methods=['GET', 'POST'])
@login_required
def book_date_time(user_id):
    if request.method == 'POST':
        test_date = request.form['testDate']
        test_time = request.form['testTime']
        collection.update_one(
            {'_id': ObjectId(user_id)},
            {'$set': {'testDate': test_date, 'testTime': test_time, 'status': 'Under Review'}}
        )
        return redirect(url_for('application_status', user_id=user_id))
    return render_template('book_date_time.html', user_id=user_id)

@app.route('/status/<user_id>')
@login_required
def application_status(user_id):
    user = collection.find_one({'_id': ObjectId(user_id)})
    return render_template('status.html', user=user)

@app.route('/rto')
@login_required
def rto_dashboard():
    applications = list(collection.find({"status": "Under Review"}))
    return render_template('rto_dashboard.html', applications=applications)

@app.route('/rto/update_status', methods=['POST'])
@login_required
def update_status():
    app_id = request.form.get('app_id')
    status = request.form.get('status')
    collection.update_one({'_id': ObjectId(app_id)}, {'$set': {'status': status}})
    return redirect(url_for('rto_dashboard'))

@app.route('/apply_for_dl')
@login_required
def apply_for_dl():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
