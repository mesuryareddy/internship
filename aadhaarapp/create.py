from pymongo import MongoClient

# Create a MongoClient to the running mongod instance
client = MongoClient("mongodb://localhost:27017/")


# Create or switch to the students database
db = client["aadhaar_app"]

# Create or switch to the student_info collection
collection = db["aadhaar"]


# Sample data
aadhaar = [
  {
    "aadhaar_number": "123456789012",
    "name": "John Doe",
    "email": "john@example.com",
    "phone": "1234567890",
    "address": "123 Main St, City, Country"
  },
  {
    "aadhaar_number": "987654321098",
    "name": "Jane Smith",
    "email": "jane@example.com",
    "phone": "0987654321",
    "address": "456 Elm St, City, Country"
  }
]

# Insert data into the collection
collection.insert_many(aadhaar)