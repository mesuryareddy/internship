# app.py

from flask import Flask, render_template, request, redirect, url_for, jsonify, flash
from pymongo import MongoClient
from bson.objectid import ObjectId
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

app = Flask(__name__)
app.secret_key = 'your_secret_key'  # Required for session management

client = MongoClient("mongodb://localhost:27017/")
db = client["demographics"]
collection = db["citizens"]

# Flask-Login setup
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class User(UserMixin):
    def __init__(self, user_id, username, password):
        self.id = user_id
        self.username = username
        self.password = password

@login_manager.user_loader
def load_user(user_id):
    user_data = collection.find_one({"_id": ObjectId(user_id)})
    if user_data:
        return User(user_id=str(user_data["_id"]), username=user_data["username"], password=user_data["password"])
    return None

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        user_data = collection.find_one({"username": username})
        if user_data and user_data['password'] == password:
            user = User(user_id=str(user_data["_id"]), username=user_data["username"], password=user_data["password"])
            login_user(user)
            return redirect(url_for('dashboard'))
        else:
            flash('Invalid username or password')
    return render_template('login.html')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html', username=current_user.username)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/get_details', methods=['POST'])
def get_details():
    aadhaar_number = request.form['aadhaarNumber']
    user = collection.find_one({"aadhaar_number": aadhaar_number})
    if user:
        user['_id'] = str(user['_id'])
        return jsonify(user)
    return jsonify({"error": "User not found"})

@app.route('/book_date_time/<user_id>', methods=['GET', 'POST'])
@login_required
def book_date_time(user_id):
    if request.method == 'POST':
        test_date = request.form['testDate']
        test_time = request.form['testTime']
        collection.update_one(
            {'_id': ObjectId(user_id)},
            {'$set': {'testDate': test_date, 'testTime': test_time, 'status': 'Under Review'}}
        )
        return redirect(url_for('application_status', user_id=user_id))
    return render_template('book_date_time.html', user_id=user_id)

@app.route('/status/<user_id>')
@login_required
def application_status(user_id):
    user = collection.find_one({'_id': ObjectId(user_id)})
    return render_template('status.html', user=user)

@app.route('/rto')
@login_required
def rto_dashboard():
    applications = list(collection.find({"status": "Under Review"}))
    return render_template('rto_dashboard.html', applications=applications)

@app.route('/rto/update_status', methods=['POST'])
@login_required
def update_status():
    app_id = request.form.get('app_id')
    status = request.form.get('status')
    collection.update_one({'_id': ObjectId(app_id)}, {'$set': {'status': status}})
    return redirect(url_for('rto_dashboard'))

if __name__ == '__main__':
    app.run(debug=True)
