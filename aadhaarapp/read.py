from pymongo import MongoClient

def main():
    # Connect to the MongoDB - Adjust the connection string as needed
    client = MongoClient("mongodb://localhost:27017/")
    
    # Select the 'aadhaar_app' database
    db = client['aadhaar_app']
     
    # Select the 'aadhaar' collection
    collection = db['aadhaar']
    
    # Retrieve and print all records from the collection
    print("aadhaar Information:")
    for aadhaar in collection.find():
        print(aadhaar)

if __name__ == "__main__":
    main()
