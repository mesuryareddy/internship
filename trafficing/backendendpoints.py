from flask import Flask, jsonify, request
from bson import ObjectId
from pymongo import MongoClient

app = Flask(__name__)

# Assuming `slots_collection` is already defined somewhere in your code
# Replace `YOUR_MONGODB_URI` with your MongoDB connection URI
client = MongoClient('mongodb://localhost:27017/')
db = client['driving_license']
slots_collection = db['slots']

@app.route('/api/getApplications', methods=['GET'])
def get_applications():
    applications = list(slots_collection.find({"status": {"$in": ["Waiting for test date", "Under review"]}}))
    for app in applications:
        app['_id'] = str(app['_id'])  # Convert ObjectId to string for JSON serialization
    return jsonify({"applications": applications})

@app.route('/api/updateStatus/<id>', methods=['POST'])
def update_status(id):
    status = request.json['status']
    result = slots_collection.update_one({"_id": ObjectId(id)}, {"$set": {"status": status}})
    return jsonify({"success": result.modified_count > 0})

@app.route('/api/getHistory', methods=['GET'])
def get_history():
    # Add your code for retrieving history here
    return jsonify({"message": "Endpoint under construction"})  # Placeholder response

if __name__ == '__main__':
    app.run(debug=True)