from flask import Flask, request, jsonify
from pymongo import MongoClient

app = Flask(__name__)
client = MongoClient('mongodb://localhost:27017/')
db = client['driving_license']
users_collection = db['users']
slots_collection = db['slots']

@app.route('/api/getDetails/<aadhaar_number>', methods=['GET'])
def get_details(aadhaar_number):
    user = users_collection.find_one({"aadhaar_number": aadhaar_number})
    if user:
        return jsonify({
            "success": True,
            "name": user['name'],
            "email": user['email'],
            "phone": user['phone'],
            "address": user['address']
        })
    else:
        return jsonify({"success": False})

@app.route('/api/bookSlot', methods=['POST'])
def book_slot():
    data = request.json
    slot = {
        "aadhaar_number": data['aadhaar-number'],
        "rto_office": data['rto-office'],
        "date": data['date'],
        "time": data['time'],
        "status": "Waiting for test date"
    }
    slots_collection.insert_one(slot)
    return jsonify({"success": True})

if __name__ == '__main__':
    app.run(debug=True)