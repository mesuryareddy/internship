from flask import Flask, render_template, request, redirect, url_for, session, flash
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.secret_key = 'your_secret_key'

app.config["MONGO_URI"] = "mongodb://localhost:27017/vehicle_db"
mongo = PyMongo(app)

@app.route('/')
def home():
    return render_template('base.html')

@app.route('/citizen_login')
def citizen_login():
    return render_template('citizen_login.html')

@app.route('/police_login')
def police_login():
    return render_template('police_login.html')

@app.route('/login', methods=['POST'])
def login():
    role = request.form['role']
    identifier = request.form['identifier']
    password = request.form['password']
    
    user = mongo.db.users.find_one({"identifier": identifier, "role": role})
    if user and check_password_hash(user['password'], password):
        session['user_id'] = str(user['_id'])
        session['role'] = role
        if role == 'police':
            return redirect(url_for('police_dashboard'))
        else:
            return redirect(url_for('citizen_dashboard'))
    flash('Invalid credentials')
    return redirect(url_for('home'))

@app.route('/citizen_register')
def citizen_register():
    return render_template('citizen_register.html')

@app.route('/police_register')
def police_register():
    return render_template('police_register.html')

@app.route('/register', methods=['POST'])
def register_user():
    role = request.form['role']
    identifier = request.form['identifier']
    password = generate_password_hash(request.form['password'])
    
    mongo.db.users.insert_one({
        "role": role,
        "identifier": identifier,
        "password": password,
        "points": 0
    })
    flash('Registration successful')
    return redirect(url_for('home'))

@app.route('/police_dashboard')
def police_dashboard():
    if session.get('role') != 'police':
        return redirect(url_for('home'))
    return render_template('police_dashboard.html')

@app.route('/citizen_dashboard')
def citizen_dashboard():
    if session.get('role') != 'citizen':
        return redirect(url_for('home'))
    user_id = session['user_id']
    user = mongo.db.users.find_one({"_id": ObjectId(user_id)})
    return render_template('citizen_dashboard.html', user=user)

@app.route('/check_vehicle', methods=['POST'])
def check_vehicle():
    if session.get('role') != 'police':
        return redirect(url_for('home'))
    
    vehicle_number = request.form['vehicle_number']
    license_check = request.form['license_check'] == 'yes'
    rc_check = request.form['rc_check'] == 'yes'
    insurance_check = request.form['insurance_check'] == 'yes'
    pollution_check = request.form['pollution_check'] == 'yes'
    
    points = 0
    if license_check and rc_check and insurance_check and pollution_check:
        points = 5
    
    vehicle = mongo.db.vehicles.find_one({"vehicle_number": vehicle_number})
    
    if vehicle:
        mongo.db.vehicles.update_one(
            {"vehicle_number": vehicle_number},
            {"$set": {
                "license_check": license_check,
                "rc_check": rc_check,
                "insurance_check": insurance_check,
                "pollution_check": pollution_check,
                "points": points
            }}
        )
    else:
        mongo.db.vehicles.insert_one({
            "vehicle_number": vehicle_number,
            "license_check": license_check,
            "rc_check": rc_check,
            "insurance_check": insurance_check,
            "pollution_check": pollution_check,
            "points": points
        })
    
    citizen = mongo.db.users.find_one({"identifier": vehicle_number, "role": "citizen"})
    if citizen and points == 5:
        mongo.db.users.update_one(
            {"_id": ObjectId(citizen['_id'])},
            {"$inc": {"points": 5}}
        )
    police_id = session['user_id']
    mongo.db.users.update_one(
        {"_id": ObjectId(police_id)},
        {"$inc": {"points": 1}}
    )
    flash('Vehicle check updated')
    return redirect(url_for('police_dashboard'))

if __name__ == '__main__':
    app.run(debug=True)
